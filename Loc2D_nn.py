import inspect
import math
import os
import sys
from time import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split
from torch.autograd import Variable
from torch.utils.data import DataLoader, TensorDataset

from Networks.Net_Drop_Relu import Net
from utils.nn_utils import NeuralNetworkWrapper


def data_load(use_cuda):
    folders = ['chodba', 'SE7.105', 'SE7.106']

    x = set()
    y = set()
    files = list()

    for folder in folders:
        files.extend([x for x in os.listdir('data_2D/' + folder) if x.endswith('.txt')])

    for file in files:
        values = file.replace('dist.', '').replace('m.txt', '').split('mX')
        if len(values) == 2:
            y.add(float(values[1]))
        else:
            y.add(0.9)

        x.add(float(values[0]))

    dfs = []

    # Data loading
    for folder in folders:
        for dist_y in y:
            for dist_x in x:
                if dist_x.is_integer():
                    fname = 'data_2D/{0}/dist.{1:.0f}mX{2:.1f}m.txt'.format(folder, dist_x, dist_y)
                else:
                    fname = 'data_2D/{0}/dist.{1:.1f}mX{2:.1f}m.txt'.format(folder, dist_x, dist_y)

                if os.path.isfile(fname):
                    df = pd.read_csv(fname, header=None, names=['time',
                                                                'tag id',
                                                                'anchor id',
                                                                'anchor1 id',
                                                                'anchor2 id',
                                                                'anchor3 id',
                                                                'a0ch0',
                                                                'a0ch1',
                                                                'a0ch2',
                                                                'a0ch3',
                                                                'a0rssi0',
                                                                'a0rssi1',
                                                                'a0rssi2',
                                                                'a0rssi3',
                                                                'a1ch0',
                                                                'a1ch1',
                                                                'a1ch2',
                                                                'a1ch3',
                                                                'a1rssi0',
                                                                'a1rssi1',
                                                                'a1rssi2',
                                                                'a1rssi3',
                                                                'a2ch0',
                                                                'a2ch1',
                                                                'a2ch2',
                                                                'a2ch3',
                                                                'a2rssi0',
                                                                'a2rssi1',
                                                                'a2rssi2',
                                                                'a2rssi3',
                                                                'a3ch0',
                                                                'a3ch1',
                                                                'a3ch2',
                                                                'a3ch3',
                                                                'a3rssi0',
                                                                'a3rssi1',
                                                                'a3rssi2',
                                                                'a3rssi3'])
                    df['dist_y'] = dist_y
                    df['dist_x'] = dist_x
                    dfs.append(df)

    df = pd.concat(dfs)
    df = df.drop(labels=['time', 'tag id', 'anchor id', 'anchor1 id', 'anchor2 id', 'anchor3 id'],
                 axis=1)

    # Data split
    X_train, X_test, y_train, y_test = train_test_split(df.drop(axis=1,
                                                                labels=['dist_y', 'dist_x']),
                                                        df.loc[:, 'dist_y':'dist_x'],
                                                        test_size=0.15,
                                                        random_state=0)

    # Test and Train dataset preparations
    torch_X_train = torch.tensor(X_train.values, dtype=torch.float)
    torch_X_test = torch.tensor(X_test.values, dtype=torch.float)
    torch_Y_train = torch.tensor(y_train.values, dtype=torch.float)
    torch_Y_test = torch.tensor(y_test.values, dtype=torch.float)

    # If use of CUDA is allowed, transfer all data to the GPU
    if use_cuda:
        torch_X_train = torch_X_train.cuda()
        torch_X_test = torch_X_test.cuda()
        torch_Y_train = torch_Y_train.cuda()
        torch_Y_test = torch_Y_test.cuda()

    train_dataset = TensorDataset(torch_X_train, torch_Y_train)
    test_dataset = TensorDataset(torch_X_test, torch_Y_test)

    return (train_dataset, test_dataset)


def evaluation_log(target, output, time):
    print('Expected/Predicted: y: {:7.4f}m/{:7.4f}m x: {:7.4f}m/{:7.4f}m Difference: '
          'y: {:8.4f}cm x: {:8.4f}cm Time: {:.4f}s'.format(target[0][0].item(),
                                                           output[0][0].item(),
                                                           target[0][1].item(),
                                                           output[0][1].item(),
                                                           abs(target[0][0].item() -
                                                               output[0][0].item()) * 100,
                                                           abs(target[0][1].item() -
                                                               output[0][1].item()) * 100,
                                                           time))


def plot_scatters(x_expected, x_predicted, y_expected, y_predicted):
    # Plotting the predicted distances vs real distances:
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle('2D positioning')

    ax1.scatter(x_expected, x_predicted, marker='x', alpha=0.2)
    ax1.plot([0, max(x_expected)], [0, max(x_expected)], 'r')
    ax1.set(xlabel='Real distance [m]', ylabel='Predicted distance [m]', title='x')
    ax1.grid(True)
    ax2.scatter(y_expected, y_predicted, marker='x', alpha=0.2)
    ax2.plot([0, max(y_expected)], [0, max(y_expected)], 'r')
    ax2.set(xlabel='Real distance [m]', ylabel='Predicted distance [m]', title='y')
    ax2.grid(True)
    plt.show()


def plot_histograms(x_expected, x_predicted, y_expected, y_predicted):
    # Plotting the histograms
    print('x')
    err_x = pd.Series(x_expected - x_predicted)
    print(err_x.describe())
    srterr_x = sorted(abs(err_x))

    print('y')
    err_y = pd.Series(y_expected - y_predicted)
    print(err_y.describe())
    srterr_y = sorted(abs(err_y))

    print('Difference')
    euclid = np.sqrt(np.power(x_expected - x_predicted, 2) + np.power(y_expected - y_predicted, 2))
    err = pd.Series(euclid)
    print(err.describe())
    srterr = sorted(abs(err))

    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    fig.suptitle('2D positioning')
    ax1.hist(srterr_x, bins=1000, cumulative=True, density=True)
    ax1.set(title='x')
    ax2.hist(srterr_y, bins=1000, cumulative=True, density=True)
    ax2.set(title='y')
    ax3.hist(srterr, bins=1000, cumulative=True, density=True)
    ax3.set(title='Difference')
    plt.show()


def plot_map(x_expected, x_predicted, y_expected, y_predicted):
    # Plotting prediction map
    img = plt.imread('hall.jpg')
    fig, ax = plt.subplots()
    ax.imshow(img, extent=[37, -3, -3, 11])
    ax.scatter(x_predicted, y_predicted, marker='x', alpha=0.2)
    ax.scatter(x_expected, y_expected, marker='x')
    ax.set(xlabel='x', ylabel='y', title='2D positioning prediction map')
    ax.grid(True)
    plt.show()


def plot_3d_map(x_expected, x_predicted, y_expected, y_predicted):
    # Plotting prediction map
    fig = plt.figure()
    ax = Axes3D(fig)

    coords = list(set([(x_expected[i], y_expected[i]) for i in range(0, len(x_expected))]))

    x0 = [x[0] for x in list(coords)]
    y0 = [x[1] for x in list(coords)]
    z0 = [0 for i in range(len(coords))]

    z1 = np.sqrt(np.power(x_expected - x_predicted, 2) + np.power(y_expected - y_predicted, 2))
    ax.scatter(x0, y0, z0, marker='x')
    ax.scatter(x_expected, y_expected, z1, marker='x', alpha=0.2)
    ax.invert_xaxis()
    ax.set(xlabel='x', ylabel='y', title='2D positioning prediction map')
    plt.show()


def main():
    # Check CUDA availability
    use_cuda = torch.cuda.is_available()

    # Load data
    train_dataset, test_dataset = data_load(use_cuda)

    # Get network name to use for backups and model names
    net_name = inspect.getmodule(Net).__name__.split('.')[1]

    #
    # Create instance of the network and set all parameters
    #

    network = NeuralNetworkWrapper(Net, (1, 32), 32)
    print(network)
    network.set_optimizer(optim.SGD(network.model.parameters(), lr=0.0001, momentum=0.9))
    network.set_loss_function(nn.MSELoss())
    network.set_backup('backup_{}'.format(net_name), 'model_{}'.format(net_name), 5)
    network.set_log_interval(2000)
    network.set_training_parameters(batch_size=20, epochs=200)
    network.load()

    if use_cuda:
        network.cuda()

    # Create data loaders
    train_loader = DataLoader(train_dataset, network.batch_size, drop_last=True, shuffle=True)
    test_loader = DataLoader(test_dataset)

    #
    # Train the network
    #

    if network.continue_training:
        network.train(train_loader)

    #
    # Evaluate the network
    #

    test_loss, expected, predicted = network.eval(test_loader, True, evaluation_log)

    #
    # Result analysis
    #

    if use_cuda:
        expected = expected.cpu()
        predicted = predicted.cpu()

    y_expe = expected[:, 0].numpy()
    y_pred = predicted[:, 0].numpy()
    x_expe = expected[:, 1].numpy()
    x_pred = predicted[:, 1].numpy()
    y_diff = abs(x_expe - x_pred)
    x_diff = abs(y_expe - y_pred)

    print('\nTest set: Average loss: {:.4f}\n'.format(test_loss))
    print('Average difference: x: {:7.4f}cm y: {:7.4f}cm\n'.format(sum(x_diff) / len(x_diff),
                                                                   sum(y_diff) / len(y_diff)))

    plot_scatters(x_expe, x_pred, y_expe, y_pred)
    plot_histograms(x_expe, x_pred, y_expe, y_pred)
    plot_map(x_expe, x_pred, y_expe, y_pred)
    plot_3d_map(x_expe, x_pred, y_expe, y_pred)


if __name__ == '__main__':
    main()
