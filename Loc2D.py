import inspect
import os
import sys
from time import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from sklearn.model_selection import train_test_split
from torch.autograd import Variable
from torch.utils.data import DataLoader, TensorDataset
from torchsummary import summary

from Networks.Net_2_Norm_Drop_Relu_remBN_lrem_drop_30_MSE import Net


def main():
    folders = ['chodba', 'SE7.105', 'SE7.106']

    x = set()
    y = set()
    files = list()

    for folder in folders:
        files.extend([x for x in os.listdir('data_2D/' + folder) if x.endswith('.txt')])

    for file in files:
        values = file.replace('dist.', '').replace('m.txt', '').split('mX')
        if len(values) == 2:
            x.add(float(values[1]))
        else:
            x.add(0.9)

        y.add(float(values[0]))

    dfs = []

    # Data loading
    for folder in folders:
        for dist_y in y:
            for dist_x in x:
                if dist_y.is_integer():
                    fname = 'data_2D/{0}/dist.{1:.0f}mX{2:.1f}m.txt'.format(folder, dist_y, dist_x)
                else:
                    fname = 'data_2D/{0}/dist.{1:.1f}mX{2:.1f}m.txt'.format(folder, dist_y, dist_x)

                if os.path.isfile(fname):
                    df = pd.read_csv(fname, header=None, names=['time',
                                                                'tag id',
                                                                'anchor id',
                                                                'anchor1 id',
                                                                'anchor2 id',
                                                                'anchor3 id',
                                                                'a0ch0',
                                                                'a0ch1',
                                                                'a0ch2',
                                                                'a0ch3',
                                                                'a0rssi0',
                                                                'a0rssi1',
                                                                'a0rssi2',
                                                                'a0rssi3',
                                                                'a1ch0',
                                                                'a1ch1',
                                                                'a1ch2',
                                                                'a1ch3',
                                                                'a1rssi0',
                                                                'a1rssi1',
                                                                'a1rssi2',
                                                                'a1rssi3',
                                                                'a2ch0',
                                                                'a2ch1',
                                                                'a2ch2',
                                                                'a2ch3',
                                                                'a2rssi0',
                                                                'a2rssi1',
                                                                'a2rssi2',
                                                                'a2rssi3',
                                                                'a3ch0',
                                                                'a3ch1',
                                                                'a3ch2',
                                                                'a3ch3',
                                                                'a3rssi0',
                                                                'a3rssi1',
                                                                'a3rssi2',
                                                                'a3rssi3'])
                    df['dist_x'] = dist_x
                    df['dist_y'] = dist_y
                    dfs.append(df)

    df = pd.concat(dfs)
    df = df.drop(labels=['time', 'tag id', 'anchor id', 'anchor1 id', 'anchor2 id', 'anchor3 id'],
                 axis=1)

    # Data split
    X_train, X_test, y_train, y_test = train_test_split(df.drop(axis=1,
                                                                labels=['dist_x', 'dist_y']),
                                                        df.loc[:, 'dist_x':'dist_y'],
                                                        test_size=0.15,
                                                        random_state=0)

    # Test and Train dataset preparations
    torch_X_train = torch.tensor(X_train.values, dtype=torch.float)
    torch_X_test = torch.tensor(X_test.values, dtype=torch.float)
    torch_Y_train = torch.tensor(y_train.values, dtype=torch.float)
    torch_Y_test = torch.tensor(y_test.values, dtype=torch.float)

    # Use CUDA if wanted and available in system (Don't, network is too small for benefits of GPU)
    use_cuda = True and torch.cuda.is_available()

    # If use of CUDA is allowed, transfer all data to the GPU
    if use_cuda:
        torch_X_train = torch_X_train.cuda()
        torch_X_test = torch_X_test.cuda()
        torch_Y_train = torch_Y_train.cuda()
        torch_Y_test = torch_Y_test.cuda()

    train_dataset = TensorDataset(torch_X_train, torch_Y_train)
    test_dataset = TensorDataset(torch_X_test, torch_Y_test)

    # Get network name to use for backups and model names
    net_name = inspect.getmodule(Net).__name__.split('.')[1]

    # Model training parameters
    continue_training = False
    batch_size = 20  # 1 is slow, but results are most accurate, 20 is faster, but not as accurate
    learning_rate = 0.00001
    epochs = 200
    log_interval = 2000 / batch_size
    backup = 5  # After how many epochs the weights backup should be made
    backup_folder = 'backup_{}'.format(net_name)
    model_name = 'model_{}'.format(net_name)
    model_path = '{}/{}_ep{}.pth'.format(backup_folder, model_name, epochs)

    # Create backup folder if it does not exist
    if not os.path.exists(backup_folder):
        os.mkdir(backup_folder)

    # Create instance of the network
    model = Net()
    summary(model, (1, 32), device='cpu')

    # Create a Stochastic Gradient Descent optimizer
    optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=0.9)
    # Create a Mean Squared Error loss function
    criterion = nn.MSELoss()

    # Load the latest weights and continue training
    if continue_training:
        files = [backup_folder + '/' + x for x in os.listdir(backup_folder) if x.endswith('.pth')]
        newest = max(files, key=os.path.getctime)
        model.load_state_dict(torch.load(newest))
        init_epoch = int(newest.replace('{}/{}_ep'.format(backup_folder,
                                                          model_name),
                                        '').replace('.pth',
                                                    ''))
    else:
        init_epoch = 0

    # Move model to GPU
    if use_cuda:
        model.cuda()

    train_loader = DataLoader(train_dataset, batch_size, drop_last=True, shuffle=True)
    test_loader = DataLoader(test_dataset)

    if not os.path.exists(model_path):
        # Set model to training mode
        model.train()
        # Run the main training loop
        for epoch in range(init_epoch, epochs):
            for batch_idx, (data, target) in enumerate(train_loader):
                # Resize data according to batch_size and last input batch_size
                target = target.view(data.shape[0], -1)

                # Zero the parameter gradients
                optimizer.zero_grad()

                # Forward + Backward + Optimize
                net_out = model(data)

                loss = criterion(net_out, target)
                loss.backward()
                optimizer.step()

                # Print statistics
                if batch_idx % log_interval == 0:
                    print('Train Epoch: {} [{}/{} ({:.0f}%)]'
                          '\tLoss: {:.6f}'.format(epoch, batch_idx * len(data),
                                                  len(train_loader.dataset),
                                                  100. * batch_idx / len(train_loader),
                                                  loss.item()))
            # Save backup of weights
            if epoch % backup == 0 and epoch != 0:
                torch.save(model.state_dict(), '{}\\{}_ep{}.pth'.format(backup_folder,
                                                                        model_name,
                                                                        epoch))

        # Save the trained model
        torch.save(model.state_dict(), model_path)
    else:
        # Load the trained model
        model.load_state_dict(torch.load(model_path))

    # Set model to evaluation mode
    model.eval()

    x_expe = list()
    x_pred = list()
    y_expe = list()
    y_pred = list()
    x_diff = list()
    y_diff = list()

    # Run a test loop
    test_loss = 0
    with torch.no_grad():
        for id, (data, target) in enumerate(test_loader):
            # Resize data according to batch_size and last input batch_size
            target = target.view(data.shape[0], -1)
            start_time = time()
            net_out = model(data)
            end_time = time()
            # Sum up batch loss
            test_loss += criterion(net_out, target).item()
            print('Expected/Predicted: x: {:7.4f}m/{:7.4f}m y: {:7.4f}m/{:7.4f}m Difference: '
                  'x: {:7.4f}cm y: {:7.4f}cm Time: {}s'.format(target[0][0].item(),
                                                               net_out[0][0].item(),
                                                               target[0][1].item(),
                                                               net_out[0][1].item(),
                                                               abs(target[0][0].item() -
                                                                   net_out[0][0].item()) * 100,
                                                               abs(target[0][1].item() -
                                                                   net_out[0][1].item()) * 100,
                                                               end_time - start_time))

            x_expe.append(target[0][0].item())
            x_pred.append(net_out[0][0].item())
            x_diff.append(abs(target[0][0].item() - net_out[0][0].item()) * 100)
            y_expe.append(target[0][1].item())
            y_pred.append(net_out[0][1].item())
            y_diff.append(abs(target[0][1].item() - net_out[0][1].item()) * 100)

    test_loss /= len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}\n'.format(test_loss))
    print('Average difference: x: {:7.4f}cm y: {:7.4f}cm\n'.format(sum(x_diff) / len(x_diff),
                                                                   sum(y_diff) / len(y_diff)))

    # Plotting the predicted distances vs real distances:
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle('2D positioning')

    ax1.scatter(x_expe, x_pred, marker='x', alpha=0.2)
    ax1.set(xlabel='Real distance [m]', ylabel='Predicted distance [m]', title='x')
    ax1.grid(True)
    ax2.scatter(y_expe, y_pred, marker='x', alpha=0.2)
    ax2.set(xlabel='Real distance [m]', ylabel='Predicted distance [m]', title='y')
    ax2.grid(True)
    plt.show()

    # Plotting the histograms
    print('x')
    err_x = pd.Series(np.asarray(x_expe) - np.asarray(x_pred))
    print(err_x.describe())
    srterr_x = sorted(abs(err_x))

    print('y')
    err_y = pd.Series(np.asarray(y_expe) - np.asarray(y_pred))
    print(err_y.describe())
    srterr_y = sorted(abs(err_y))

    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.hist(srterr_x, bins=1000, cumulative=True, density=True)
    ax1.set(title='x')
    ax2.hist(srterr_y, bins=1000, cumulative=True, density=True)
    ax2.set(title='y')
    plt.show()

    fig, ax = plt.subplots()
    ax.scatter(x_pred, y_pred, marker='x', alpha=0.2)
    ax.scatter(x_expe, y_expe, marker='o')
    ax.set(xlabel='x', ylabel='y', title='2D positioning prediction map')
    ax.grid(True)
    plt.show()


if __name__ == '__main__':
    main()
