import inspect
import os
import sys
from time import time

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from sklearn.model_selection import train_test_split
from torch.autograd import Variable
from torch.utils.data import DataLoader, TensorDataset
from .torchsummary import summary_string


class NeuralNetworkWrapper():
    def __init__(self, network, input_size, network_param=None):
        if network_param is not None:
            self.model = network(network_param)
        else:
            self.model = network()

        self.input_size = input_size

        self.batch_size = 20
        self.init_epoch = 0
        self.epochs = 100
        self.log_interval = 100
        self.continue_training = False

        self.optimizer = None
        self.criterion = None

        self.device = 'cpu'

        self.backup_dir = None
        self.backup_filename = None
        self.backup_interval = 10

    def __str__(self):
        '''Returns string containing the network architecture'''
        return summary_string(self.model, self.input_size, device=self.device)[0]

    def __call__(self, value):
        '''Forward pass through the network'''
        return self.model(value)

    def set_training_parameters(self, batch_size=20, epochs=100):
        '''Sets basic training parameters'''
        if self.batch_size != batch_size:
            interval = self.log_interval * self.batch_size
            self.batch_size = batch_size
            self.log_interval = interval / self.batch_size

        self.epochs = epochs

    def set_log_interval(self, interval):
        '''Sets how often should the training log to be displayed

        log_interval = interval / batch_size
        '''
        self.log_interval = interval / self.batch_size

    def set_optimizer(self, optimizer):
        '''Sets the optimizer to be used in training'''
        self.optimizer = optimizer

    def set_loss_function(self, loss_function):
        '''Sets the loss function to be used'''
        self.criterion = loss_function

    def cuda(self, id=0):
        '''Moves all model parameters and buffers to the GPU.'''
        if torch.cuda.is_available():
            self.device = 'cuda:{}'.format(id)
            self.model.to(device=self.device)
        else:
            print('There is no CUDA capable device available!')

    def cpu(self):
        '''Moves all model parameters and buffers to the CPU.'''
        self.device = 'cpu'
        self.model.to(device=self.device)

    def load(self, weights_path=None, epoch_func=None):
        '''Loads weights for the model from file

        if weights_path = None, and backup is set using set_backup function
        loads the last file from backup folder with .pth extension

        if epoch_func != None init_epoch is deduced from file names in backup
        directory with epoch_func, which is supposed to contain a filename
        parser whitch returns an integer value
        '''
        if weights_path is not None and os.path.exists(weights_path):
            self.model.load_state_dict(torch.load(weights_path))
        else:
            if self.backup_dir is None or self.backup_filename is None:
                print('Backup is not configured! Weights have not been loaded.')
                return

            files = [x for x in os.listdir(self.backup_dir) if x.endswith('.pth')]
            files = [self.backup_dir + '/' + x for x in files]

            if files:
                newest = max(files, key=os.path.getctime)
                if epoch_func is not None:
                    self.init_epoch = epoch_func(newest)

                self.model.load_state_dict(torch.load(newest))
            else:
                print('Weights have not been loaded. No weights have been found in backup folder!')
                self.continue_training = True

    def save(self, weights_path):
        '''Saves the current weights of the model'''
        torch.save(self.model.state_dict(), weights_path)

    def set_backup(self, folder, filename, interval):
        '''Creates backup folder if it does not exist, and stores settings'''
        self.backup_dir = folder
        self.backup_filename = filename
        self.backup_interval = interval

        if not os.path.exists(self.backup_dir):
            os.makedirs(self.backup_dir)

    def save_backup(self, epoch=None):
        '''Saves weights into folder defined using set_backup function'''
        if self.backup_dir is not None and self.backup_filename is not None:
            if epoch is None:
                self.save('{}/{}.pth'.format(self.backup_dir, self.backup_filename))
            else:
                self.save('{}/{}_ep{}.pth'.format(self.backup_dir, self.backup_filename, epoch))
        else:
            print('Backup not saved! No backup folder and filename set!')

    def set_init_epoch(self, init_epoch=0):
        '''Enables continuation of training from init_epoch'''
        self.init_epoch = init_epoch
        self.continue_training = True

    def find_init_epoch(self, epoch_func):
        '''Enables continuation of training

        init_epoch is deduced from file names in backup directory with
        epoch_func, which is supposed to contain a filename parser whitch
        returns an integer value
        '''
        if epoch_func is None:
            print('No function parser included')
            return

        files = [x for x in os.listdir(self.backup_dir) if x.endswith('.pth')]
        files = [self.backup_dir + '/' + x for x in files]

        if files:
            newest = max(files, key=os.path.getctime)
            self.init_epoch = epoch_func(newest)

    def train(self, data_loader, continue_training=False, log_func=None):
        '''Train the model using the data in data_loader

        log_func - custom function for logging with arguments:
            - epoch
            - batch index
            - batch size
            - length of training dataset
            - loss
            - target tensor
            - output tensor
            - compute time
            - if log_func is None, default output is used
        '''
        self.model.train()
        for epoch in range(self.init_epoch, self.epochs):
            correct = 0
            for batch_idx, (data, target) in enumerate(data_loader):
                start_time = time()
                self.optimizer.zero_grad()
                net_out = self.model(data)
                loss = self.criterion(net_out, target)
                loss.backward()
                self.optimizer.step()
                end_time = time()

                net_out_classes = torch.zeros(self.batch_size, device=self.device)
                for idx, out in enumerate(net_out):
                    net_out_classes[idx] = torch.max(out, 0)[1]
                correct += (net_out_classes == target).sum().item()

                # Print log
                if batch_idx % self.log_interval == 0:
                    if log_func is not None:
                        log_func(epoch, batch_idx, len(data),
                                 len(data_loader.dataset), loss, target,
                                 net_out, end_time - start_time)
                    else:
                        print('Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}\tAccuracy: {:.4f}%'.format(
                            epoch, batch_idx * len(data), len(data_loader.dataset),
                            100 * batch_idx / len(data_loader), loss.item(),
                            correct / (batch_idx * self.batch_size + self. batch_size) * 100))

            # Save backup of weights
            if epoch % self.backup_interval == 0 and epoch != 0:
                self.save_backup(epoch)

        self.save_backup(self.epochs)

    def eval(self, data_loader, return_output=False, log_func=None):
        '''Evaluate the model using the data in data_loader

        return_output - Boolean value
            - if True returns average loss and 2 lists of predicted and expected values
            - if False only returned value is average loss of evaluation set

        log_func - function for logging with input arguments:
            - target tensor
            - output tensor
            - compute time in seconds
        '''
        self.model.eval()
        test_loss = 0

        if return_output:
            expected = torch.zeros(len(data_loader.dataset[:][1]))
            predicted = torch.zeros(len(data_loader.dataset[:][1]))

        if not self.device == 'cpu' and return_output:
            expected = expected.to(device=self.device)
            predicted = predicted.to(device=self.device)

        with torch.no_grad():
            for id, (data, target) in enumerate(data_loader):
                start_time = time()
                net_out = self.model(data)
                end_time = time()
                # Sum up batch loss
                test_loss += self.criterion(net_out, target).item()

                if log_func is not None:
                    log_func(target, net_out, end_time - start_time)

                if return_output:
                    expected[id] = target.item()
                    predicted[id] = torch.max(net_out.squeeze(), 0)[1].item()

        if return_output:
            return (test_loss / len(data_loader.dataset), expected, predicted)
        else:
            return test_loss / len(data_loader.dataset)
