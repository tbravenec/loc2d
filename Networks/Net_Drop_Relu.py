import torch
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self, input_length=32):
        self.input_length = input_length
        super(Net, self).__init__()
        self.conv1d_1 = nn.Conv1d(1, 32, 5, padding=2)
        self.acti0 = nn.ReLU()
        self.lin_1 = nn.Linear(self.conv1d_1.out_channels * input_length, 1024)
        self.acti1 = nn.ReLU()
        self.lin_2 = nn.Linear(self.lin_1.out_features, 1024)
        self.drop2 = nn.Dropout(0.3)
        self.acti2 = nn.ReLU()
        self.lin_3 = nn.Linear(self.lin_2.out_features, 1024)
        self.drop3 = nn.Dropout(0.3)
        self.acti3 = nn.ReLU()
        self.lin_4 = nn.Linear(self.lin_3.out_features, 1024)
        self.drop4 = nn.Dropout(0.3)
        self.acti4 = nn.ReLU()
        self.lin_5 = nn.Linear(self.lin_4.out_features, 512)
        self.drop5 = nn.Dropout(0.3)
        self.acti5 = nn.ReLU()
        self.lin_6 = nn.Linear(self.lin_5.out_features, 256)
        self.acti6 = nn.ReLU()
        self.lin_7 = nn.Linear(self.lin_6.out_features, 192)
        self.acti7 = nn.ReLU()
        self.lin_8 = nn.Linear(self.lin_7.out_features, 2)

    def forward(self, x):
        x = x.view(-1, 1, self.input_length)
        x = self.conv1d_1(x)
        x = self.acti0(x)

        x = x.view(x.shape[0], -1)

        x = self.lin_1(x)
        x = self.acti1(x)

        x = self.lin_2(x)
        x = self.drop2(x)
        x = self.acti2(x)

        x = self.lin_3(x)
        x = self.drop3(x)
        x = self.acti3(x)

        x = self.lin_4(x)
        x = self.drop4(x)
        x = self.acti4(x)

        x = self.lin_5(x)
        x = self.drop5(x)
        x = self.acti5(x)

        x = self.lin_6(x)
        x = self.acti6(x)

        x = self.lin_7(x)
        x = self.acti7(x)

        x = self.lin_8(x)
        return x
