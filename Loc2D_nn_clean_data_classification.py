import csv
import inspect
import math
import os
import sys
from time import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split
from torch.autograd import Variable
from torch.utils.data import DataLoader, TensorDataset

from Networks.Net_Drop_Relu_Classification import Net
from utils.nn_utils_classification import NeuralNetworkWrapper

with open('classes.csv', 'r') as input:
    csv_in = csv.reader(input, delimiter=';', lineterminator='\n')
    next(csv_in)
    classes = [tuple(map(float, x)) for x in map(tuple, csv_in)]


def data_load_csv(use_cuda, load_classes=True):
    files = ['data_2D/' + x for x in os.listdir('data_2D/') if x.endswith('.csv')]

    dfs = list()

    for file in files:
        df = pd.read_csv(file, header=0, delimiter=';')
        dfs.append(df)

    df = pd.concat(dfs, ignore_index=True)
    # Delete column 0 and time
    df = df.drop([df.columns[0]], axis='columns')
    df = df.drop(labels=['time'], axis='columns')

    # Data split
    X_train, X_test, y_train, y_test = train_test_split(df.drop(axis='columns',
                                                                labels=['dist_x', 'dist_y']),
                                                        df.loc[:, 'dist_x':'dist_y'],
                                                        test_size=0.15,
                                                        random_state=0)

    if load_classes and os.path.isfile('classes.csv'):
        with open('classes.csv', 'r') as input:
            csv_in = csv.reader(input, delimiter=';', lineterminator='\n')
            next(csv_in)
            values = [tuple(map(float, x)) for x in map(tuple, csv_in)]
    else:
        values = set()
        for i in y_train.values:
            values.add((np.around(i[0], 1), np.around(i[1], 1)))

        values = list(values)
        values = sorted(values, key=lambda element: (element[1], element[0]))

        with open('classes.csv', 'w') as out:
            csv_out = csv.writer(out, delimiter=';', lineterminator='\n')
            csv_out.writerow(['x', 'y'])
            csv_out.writerows(values)

    y_train_class = list()
    y_test_class = list()

    for train in y_train.values:
        y_train_class.append(values.index((np.around(train[0], 1), np.around(train[1], 1))))

    for test in y_test.values:
        y_test_class.append(values.index((np.around(test[0], 1), np.around(test[1], 1))))

    # Test and Train dataset preparations
    torch_X_train = torch.tensor(X_train.values, dtype=torch.float)
    torch_X_test = torch.tensor(X_test.values, dtype=torch.float)
    torch_Y_train = torch.tensor(y_train_class, dtype=torch.long)
    torch_Y_test = torch.tensor(y_test_class, dtype=torch.long)

    # If use of CUDA is allowed, transfer all data to the GPU
    if use_cuda:
        torch_X_train = torch_X_train.cuda()
        torch_X_test = torch_X_test.cuda()
        torch_Y_train = torch_Y_train.cuda()
        torch_Y_test = torch_Y_test.cuda()

    train_dataset = TensorDataset(torch_X_train, torch_Y_train)
    test_dataset = TensorDataset(torch_X_test, torch_Y_test)

    return (train_dataset, test_dataset)


def evaluation_log(target, output, time):
    print('Expected/Predicted: {}/{}'.format(classes[target.item()],
                                             classes[torch.max(output.squeeze(), 0)[1].item()]))


def plot_scatters_classification(expected, predicted, save_name=None):
    # Plotting the predicted distances vs real distances:
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.scatter([classes[int(i.item())][0] for i in expected],
                [classes[int(i.item())][0] for i in predicted], marker='x', alpha=.1)
    ax1.set_xlabel('Real distance $x$  [m]')
    ax1.set_ylabel('Predicted distance $x$ [m]')
    ax1.set_xticks(ticks=[0, 5, 10, 15, 20, 25, 30, 35])
    ax1.set_xticklabels(labels=['0', '5', '10', '15', '20', '25', '30', '35'])
    ax1.set_yticks(ticks=[0, 5, 10, 15, 20, 25, 30, 35])
    ax1.set_yticklabels(labels=['0', '5', '10', '15', '20', '25', '30', '35'])

    ax2.scatter([classes[int(i.item())][1] for i in expected],
                [classes[int(i.item())][1] for i in predicted], marker='x', alpha=.1)
    ax2.set_xlabel('Real distance $y$  [m]')
    ax2.set_ylabel('Predicted distance $y$ [m]')
    ax2.set_xticks(ticks=[0, 2, 4, 6, 8])
    ax2.set_xticklabels(labels=['0', '2', '4', '6', '8'])
    ax2.set_yticks(ticks=[0, 2, 4, 6, 8])
    ax2.set_yticklabels(labels=['0', '2', '4', '6', '8'])

    if save_name is not None:
        plt.savefig(save_name, bbox_inches='tight')

    plt.show()


def main():
    # Check CUDA availability
    use_cuda = torch.cuda.is_available()

    # Load data
    train_dataset, test_dataset = data_load_csv(use_cuda)

    # Get network name to use for backups and model names
    net_name = inspect.getmodule(Net).__name__.split('.')[1]

    #
    # Create instance of the network and set all parameters
    #

    network = NeuralNetworkWrapper(Net, (1, 12), 12)
    print(network)
    network.set_optimizer(optim.SGD(network.model.parameters(), lr=0.001, momentum=0.9))
    network.set_loss_function(nn.CrossEntropyLoss())
    network.set_backup('backup_clean_class_{}'.format(net_name),
                       'model_clean_class_{}'.format(net_name), 5)
    network.set_log_interval(2000)
    network.set_training_parameters(batch_size=20, epochs=75)
    network.load()

    if use_cuda:
        network.cuda()

    # Create data loaders
    train_loader = DataLoader(train_dataset, network.batch_size, drop_last=True, shuffle=True)
    test_loader = DataLoader(test_dataset)

    #
    # Train the network
    #

    if network.continue_training:
        network.train(train_loader)

    #
    # Evaluate the network
    #

    test_loss, expected, predicted = network.eval(test_loader, True, evaluation_log)

    #
    # Result analysis
    #

    if use_cuda:
        expected = expected.cpu()
        predicted = predicted.cpu()

    print('\nTest set: Average loss: {:.4f}\n'.format(test_loss))
    print('Test accuracy: {}'.format((expected == predicted).sum().float() /
                                     len(expected)))
    print('Accuracy x: {}'.format(
        (torch.tensor([classes[int(i.item())][0] for i in predicted]) ==
         torch.tensor([classes[int(i.item())][0] for i in expected])).sum().float() /
        len(expected)
        ))

    print('Accuracy y: {}'.format(
        (torch.tensor([classes[int(i.item())][1] for i in predicted]) ==
         torch.tensor([classes[int(i.item())][1] for i in expected])).sum().float() /
        len(expected)
        ))

    plot_scatters_classification(expected, predicted, 'cnn_2D_class_scatter.pdf')


if __name__ == '__main__':
    main()
