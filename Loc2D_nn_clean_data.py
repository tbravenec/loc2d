import inspect
import csv
import math
import os
import sys
from time import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split
from torch.autograd import Variable
from torch.utils.data import DataLoader, TensorDataset

from Networks.Net_Drop_Relu import Net
from utils.nn_utils import NeuralNetworkWrapper

with open('classes.csv', 'r') as input:
    csv_in = csv.reader(input, delimiter=';', lineterminator='\n')
    next(csv_in)
    classes = [tuple(map(float, x)) for x in map(tuple, csv_in)]


def classify(expected, predicted, delta=0.25):
    expected_classes = list()
    predicted_classes = list()

    for expected_val, predicted_val in zip(expected, predicted):
        y_min = expected_val[0] - delta
        y_max = expected_val[0] + delta
        x_min = expected_val[1] - delta
        x_max = expected_val[1] + delta

        expected_class = classes.index((np.around(expected_val[0].item(), 1),
                                        np.around(expected_val[1].item(), 1)))

        for idx, class_dist in enumerate(classes):
            if idx == 0:
                shortest = np.sqrt(np.power(class_dist[0] - predicted_val[0], 2) +
                                   np.power(class_dist[1] - predicted_val[1], 2))
                shortest_class = classes.index((np.around(class_dist[0], 1),
                                                np.around(class_dist[1], 1)))
            else:
                distance = np.sqrt(np.power(class_dist[0] - predicted_val[0], 2) +
                                   np.power(class_dist[1] - predicted_val[1], 2))
                if distance < shortest:
                    shortest = distance
                    shortest_class = classes.index((np.around(class_dist[0], 1),
                                                    np.around(class_dist[1], 1)))

        expected_classes.append(expected_class)
        predicted_classes.append(shortest_class)

    return expected_classes, predicted_classes


def data_load_csv(use_cuda):
    files = ['data_2D/' + x for x in os.listdir('data_2D/') if x.endswith('.csv')]

    dfs = list()

    for file in files:
        df = pd.read_csv(file, header=0, delimiter=';')
        dfs.append(df)

    df = pd.concat(dfs, ignore_index=True)
    # Delete column 0 and time
    df = df.drop([df.columns[0]], axis='columns')
    df = df.drop(labels=['time'], axis='columns')

    # Data split
    X_train, X_test, y_train, y_test = train_test_split(df.drop(axis='columns',
                                                                labels=['dist_x', 'dist_y']),
                                                        df.loc[:, 'dist_x':'dist_y'],
                                                        test_size=0.15,
                                                        random_state=0)

    # Test and Train dataset preparations
    torch_X_train = torch.tensor(X_train.values, dtype=torch.float)
    torch_X_test = torch.tensor(X_test.values, dtype=torch.float)
    torch_Y_train = torch.tensor(y_train.values, dtype=torch.float)
    torch_Y_test = torch.tensor(y_test.values, dtype=torch.float)

    # If use of CUDA is allowed, transfer all data to the GPU
    if use_cuda:
        torch_X_train = torch_X_train.cuda()
        torch_X_test = torch_X_test.cuda()
        torch_Y_train = torch_Y_train.cuda()
        torch_Y_test = torch_Y_test.cuda()

    train_dataset = TensorDataset(torch_X_train, torch_Y_train)
    test_dataset = TensorDataset(torch_X_test, torch_Y_test)

    return (train_dataset, test_dataset)


def evaluation_log(target, output, time):
    print('Expected/Predicted: y: {:7.4f}m/{:7.4f}m x: {:7.4f}m/{:7.4f}m Difference: '
          'y: {:8.4f}cm x: {:8.4f}cm Time: {:.4f}s'.format(target[0][0].item(),
                                                           output[0][0].item(),
                                                           target[0][1].item(),
                                                           output[0][1].item(),
                                                           abs(target[0][0].item() -
                                                               output[0][0].item()) * 100,
                                                           abs(target[0][1].item() -
                                                               output[0][1].item()) * 100,
                                                           time))


def plot_scatters(x_expected, x_predicted, y_expected, y_predicted):
    # Plotting the predicted distances vs real distances:
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle('2D positioning - Clean Data')

    ax1.scatter(x_expected, x_predicted, marker='x', alpha=0.2)
    ax1.plot([0, max(x_expected)], [0, max(x_expected)], 'r')
    ax1.set(xlabel='Real distance [m]', ylabel='Predicted distance [m]', title='x')
    ax1.grid(True)
    ax2.scatter(y_expected, y_predicted, marker='x', alpha=0.2)
    ax2.plot([0, max(y_expected)], [0, max(y_expected)], 'r')
    ax2.set(xlabel='Real distance [m]', ylabel='Predicted distance [m]', title='y')
    ax2.grid(True)
    plt.show()


def plot_scatters_classification(expected, predicted, save_name=None):
    # Plotting the predicted distances vs real distances:
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.scatter([classes[int(i)][0] for i in expected],
                [classes[int(i)][0] for i in predicted], marker='x', alpha=.1)
    ax1.set_xlabel('Real distance $x$  [m]')
    ax1.set_ylabel('Predicted distance $x$ [m]')
    ax1.set_xticks(ticks=[0, 5, 10, 15, 20, 25, 30, 35])
    ax1.set_xticklabels(labels=['0', '5', '10', '15', '20', '25', '30', '35'])
    ax1.set_yticks(ticks=[0, 5, 10, 15, 20, 25, 30, 35])
    ax1.set_yticklabels(labels=['0', '5', '10', '15', '20', '25', '30', '35'])

    ax2.scatter([classes[int(i)][1] for i in expected],
                [classes[int(i)][1] for i in predicted], marker='x', alpha=.1)
    ax2.set_xlabel('Real distance $y$  [m]')
    ax2.set_ylabel('Predicted distance $y$ [m]')
    ax2.set_xticks(ticks=[0, 2, 4, 6, 8])
    ax2.set_xticklabels(labels=['0', '2', '4', '6', '8'])
    ax2.set_yticks(ticks=[0, 2, 4, 6, 8])
    ax2.set_yticklabels(labels=['0', '2', '4', '6', '8'])

    if save_name is not None:
        plt.savefig(save_name, bbox_inches='tight')

    plt.show()


def plot_histograms(x_expected, x_predicted, y_expected, y_predicted):
    # Plotting the histograms
    print('x')
    err_x = pd.Series(x_expected - x_predicted)
    print(err_x.describe())
    srterr_x = sorted(abs(err_x))

    print('y')
    err_y = pd.Series(y_expected - y_predicted)
    print(err_y.describe())
    srterr_y = sorted(abs(err_y))

    print('Difference')
    euclid = np.sqrt(np.power(x_expected - x_predicted, 2) + np.power(y_expected - y_predicted, 2))
    err = pd.Series(euclid)
    print(err.describe())
    srterr = sorted(abs(err))

    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    fig.suptitle('2D positioning - Clean Data')
    ax1.hist(srterr_x, bins=1000, cumulative=True, density=True)
    ax1.set(title='x')
    ax2.hist(srterr_y, bins=1000, cumulative=True, density=True)
    ax2.set(title='y')
    ax3.hist(srterr, bins=1000, cumulative=True, density=True)
    ax3.set(title='Difference')
    plt.show()


def plot_map(x_expected, x_predicted, y_expected, y_predicted):
    # Plotting prediction map
    img = plt.imread('hall.jpg')
    fig, ax = plt.subplots()
    ax.imshow(img, extent=[37, -3, -3, 11])
    ax.scatter(x_predicted, y_predicted, marker='x', alpha=0.2)
    ax.scatter(x_expected, y_expected, marker='x')
    ax.set(xlabel='x', ylabel='y', title='2D positioning prediction map - Clean Data')
    ax.grid(True)
    # ax.invert_xaxis()
    plt.show()


def plot_3d_map(x_expected, x_predicted, y_expected, y_predicted):
    # Plotting prediction map
    fig = plt.figure()
    ax = Axes3D(fig)

    coords = list(set([(x_expected[i], y_expected[i]) for i in range(0, len(x_expected))]))

    x0 = [x[0] for x in list(coords)]
    y0 = [x[1] for x in list(coords)]
    z0 = [0 for i in range(len(coords))]

    z1 = np.sqrt(np.power(x_expected - x_predicted, 2) + np.power(y_expected - y_predicted, 2))
    ax.scatter(x0, y0, z0, marker='x')
    ax.scatter(x_expected, y_expected, z1, marker='x', alpha=0.2)
    ax.invert_xaxis()
    ax.set(xlabel='x', ylabel='y', title='2D positioning prediction map - Clean Data')
    plt.show()


def main():
    # Check CUDA availability
    use_cuda = torch.cuda.is_available()

    # Load data
    train_dataset, test_dataset = data_load_csv(use_cuda)

    # Get network name to use for backups and model names
    net_name = inspect.getmodule(Net).__name__.split('.')[1]

    #
    # Create instance of the network and set all parameters
    #

    network = NeuralNetworkWrapper(Net, (1, 12), 12)
    print(network)
    network.set_optimizer(optim.SGD(network.model.parameters(), lr=0.0001, momentum=0.9))
    network.set_loss_function(nn.MSELoss())
    network.set_backup('backup_clean_{}'.format(net_name), 'model_clean_{}'.format(net_name), 5)
    network.set_log_interval(2000)
    network.set_training_parameters(batch_size=20, epochs=100)
    network.load()

    if use_cuda:
        network.cuda()

    # Create data loaders
    train_loader = DataLoader(train_dataset, network.batch_size, drop_last=True, shuffle=True)
    test_loader = DataLoader(test_dataset)

    #
    # Train the network
    #

    if network.continue_training:
        network.train(train_loader)

    #
    # Evaluate the network
    #

    test_loss, expected, predicted = network.eval(test_loader, True, evaluation_log)

    #
    # Result analysis
    #

    if use_cuda:
        expected = expected.cpu()
        predicted = predicted.cpu()

    y_expe = expected[:, 1].numpy()
    y_pred = predicted[:, 1].numpy()
    x_expe = expected[:, 0].numpy()
    x_pred = predicted[:, 0].numpy()
    y_diff = abs(x_expe - x_pred)
    x_diff = abs(y_expe - y_pred)

    print('\nTest set: Average loss: {:.4f}\n'.format(test_loss))
    print('Average difference: x: {:7.4f}cm y: {:7.4f}cm\n'.format(sum(x_diff) / len(x_diff) * 100,
                                                                   sum(y_diff) / len(y_diff) * 100))

    expected_classes, predicted_classes = classify(expected, predicted)
    print('Test accuracy: {}'.format((torch.tensor(expected_classes) ==
                                      torch.tensor(predicted_classes)).sum().float() /
                                     len(expected)))
    print('Accuracy x: {}'.format(
        (torch.tensor([classes[int(i)][0] for i in predicted_classes]) ==
         torch.tensor([classes[int(i)][0] for i in expected_classes])).sum().float() /
        len(expected)
        ))

    print('Accuracy y: {}'.format(
        (torch.tensor([classes[int(i)][1] for i in predicted_classes]) ==
         torch.tensor([classes[int(i)][1] for i in expected_classes])).sum().float() /
        len(expected)
        ))

    plot_scatters_classification(expected_classes, predicted_classes, 'cnn_2D_regress_scatter.pdf')

    plot_scatters(x_expe, x_pred, y_expe, y_pred)
    plot_histograms(x_expe, x_pred, y_expe, y_pred)
    plot_map(x_expe, x_pred, y_expe, y_pred)
    plot_3d_map(x_expe, x_pred, y_expe, y_pred)


if __name__ == '__main__':
    main()
