import os
import random

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def main():
    x = list()
    y = list()
    points = set()
    files = list()
    all_files = list()

    for folder in ['chodba', 'SE7.105', 'SE7.106']:
        files.extend([x for x in os.listdir('data_2D/' + folder) if x.endswith('.txt')])
        files = [x.replace('dist.', '').replace('m.txt', '') for x in files]

    for file in files:
        values = file.split('mX')
        points.add((float(values[1]), float(values[0])))

    x = [i[0] for i in points]
    y = [i[1] for i in points]

    img = plt.imread('hall.jpg')

    fig, ax = plt.subplots()
    ax.imshow(img, extent=[37, -3, -3, 11])
    ax.scatter(y, x, marker='x')
    ax.set(xlabel='x', ylabel='y', title='2D positioning prediction map')
    ax.grid(True)
    plt.show()


if __name__ == '__main__':
    main()
